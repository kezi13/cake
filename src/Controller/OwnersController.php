<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;

/**
 * Owners Controller
 *
 * @property \App\Model\Table\OwnersTable $Owners
 */
class OwnersController extends AppController
{
  public $uses=array('Owner','Vehicle');
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {

        $owners = $this->Owners->find('all');

        $this->set(compact('owners'));
    }

    public function customPage()
    {
      $connection = ConnectionManager::get('default');
      $results = $connection->execute('SELECT first_name,last_name,name,type,brand FROM owners_vehicles,owners, vehicles,types,brands WHERE owners.id=owners_vehicles.owner_id AND owners_vehicles.vehicle_id=vehicles.id AND vehicles.brand_id=brands.id AND vehicles.type_id=types.id;')->fetchAll('assoc');


        $this->set(compact('results'));
    }

    public function customPage2()
    {
      $user=$this->request->params['user'];

      $connection = ConnectionManager::get('default');

      $results = $connection->execute("SELECT id FROM owners WHERE first_name='$user'")->fetch('assoc');
      $id=$results['id'];
      if(!empty($id)){

          $results = $connection->execute("SELECT name FROM owners_vehicles,vehicles WHERE owners_vehicles.owner_id=$id AND owners_vehicles.vehicle_id= vehicles.id")->fetchAll('assoc');

        }
        else {

      $id=$results['id'];
  //    $results = $connection->execute("SELECT name FROM owners_vehicles,vehicles WHERE owners_vehicles.owner_id=$id AND owners_vehicles.vehicle_id= vehicles.id")->fetchAll('assoc');
      $results="";
      $id="";
    }
      $this->set(compact('results','id'));


    }


    /**
     * View method
     *
     * @param string|null $id Owner id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $owner = $this->Owners->get($id, [
            'contain' => ['Vehicles']
        ]);

        $this->set('owner', $owner);
        $this->set('_serialize', ['owner']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $owner = $this->Owners->newEntity();
        if ($this->request->is('post')) {
            $owner = $this->Owners->patchEntity($owner, $this->request->data);
            if ($this->Owners->save($owner)) {
                $this->Flash->success(__('The owner has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The owner could not be saved. Please, try again.'));
            }
        }
        $vehicles = $this->Owners->Vehicles->find('list', ['limit' => 200]);
        $this->set(compact('owner', 'vehicles'));
        $this->set('_serialize', ['owner']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Owner id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $owner = $this->Owners->get($id, [
            'contain' => ['Vehicles']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $owner = $this->Owners->patchEntity($owner, $this->request->data);
            if ($this->Owners->save($owner)) {
                $this->Flash->success(__('The owner has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The owner could not be saved. Please, try again.'));
            }
        }
        $vehicles = $this->Owners->Vehicles->find('list', ['limit' => 200]);
        $this->set(compact('owner', 'vehicles'));
        $this->set('_serialize', ['owner']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Owner id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $owner = $this->Owners->get($id);
        if ($this->Owners->delete($owner)) {
            $this->Flash->success(__('The owner has been deleted.'));
        } else {
            $this->Flash->error(__('The owner could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
