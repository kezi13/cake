<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OwnersVehiclesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OwnersVehiclesTable Test Case
 */
class OwnersVehiclesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\OwnersVehiclesTable
     */
    public $OwnersVehicles;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.owners_vehicles',
        'app.owners',
        'app.vehicles',
        'app.brands',
        'app.types'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('OwnersVehicles') ? [] : ['className' => 'App\Model\Table\OwnersVehiclesTable'];
        $this->OwnersVehicles = TableRegistry::get('OwnersVehicles', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->OwnersVehicles);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
