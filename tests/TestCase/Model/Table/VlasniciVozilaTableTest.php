<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\VlasniciVozilaTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\VlasniciVozilaTable Test Case
 */
class VlasniciVozilaTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\VlasniciVozilaTable
     */
    public $VlasniciVozila;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.vlasnici_vozila',
        'app.vlasniks',
        'app.vozilos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('VlasniciVozila') ? [] : ['className' => 'App\Model\Table\VlasniciVozilaTable'];
        $this->VlasniciVozila = TableRegistry::get('VlasniciVozila', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->VlasniciVozila);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
