<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\VozilaTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\VozilaTable Test Case
 */
class VozilaTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\VozilaTable
     */
    public $Vozila;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.vozila',
        'app.vlasnici',
        'app.vlasnici_vozila'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Vozila') ? [] : ['className' => 'App\Model\Table\VozilaTable'];
        $this->Vozila = TableRegistry::get('Vozila', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Vozila);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
