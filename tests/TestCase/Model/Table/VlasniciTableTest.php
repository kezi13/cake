<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\VlasniciTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\VlasniciTable Test Case
 */
class VlasniciTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\VlasniciTable
     */
    public $Vlasnici;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.vlasnici'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Vlasnici') ? [] : ['className' => 'App\Model\Table\VlasniciTable'];
        $this->Vlasnici = TableRegistry::get('Vlasnici', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Vlasnici);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
